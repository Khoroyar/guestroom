/**
 * Created by master on 09.11.2015.
 */

$(document).ready(function() {

    var serverUrl = "http://localhost:3000/messages";

    getAll();

    $('#sendMessageBtn').on('click', function() {
        var userName = $('#userName').val();
        var messageText = $('#messageText').val();

        $.ajax({
            url: serverUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                userName: userName,
                messageText: messageText
            }),
            success: function(data){
                addToList(data);
                console.log(data);
            },
            error: onAjaxError
        });
    });

    function addToList(data) {
        $('#roomMessages').append('<div class="row">' +
            '<div class="col-lg-12">' +
                '<blockquote>' +
                    '<p>' + data.messageText + '</p>' +
                    '<small>' + data.userName + '</small>' +
                '</blockquote>' +
            '</div>' +
        '</div>')
    }

    function getAll() {
        $.ajax({
            url: serverUrl,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function(all) {
                if (all.length && all.length != 0) {
                    all.forEach(function(one) {
                        addToList(one);
                    });
                }
            },
            error: onAjaxError
        });
    }

    function onAjaxError(err) {
        console.log(err);
        alert('Error');
    }
});